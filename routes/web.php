<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');

});
Route::get('users' , 'UserController@index');
Route::get('songs/{song}' , function (Song $song){
	return new SongResource($song);
});
Route::get('/songs', function() {
	return 'ok';
    return new SongResource(Song::all());

});
